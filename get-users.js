/* Extract User Information from iTunes U Course Manager Private Courses
 *
 * https://bitbucket.org/rowansimms/itunesu-get-private-users
 * 
 * Author: Rowan Simms <code@rowansimms.com>
 * Version: 1.0
 * Date: 20130822
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* WARNING
 * This file is the raw-source and is not in a format of immediate use
 * please used the minimised version available in the same repository
 * or minimise this file and add 'javascript:' to the beginning of the line.
 * It will then be in a format appropriate for pasting in to the Address bar
 * of the Safari web browser.
 * See the WIKI for information: 
 *    https://bitbucket.org/rowansimms/itunesu-get-private-users/wiki/Home
 */

DI = document.getElementsByClassName("student-table-cell");
var usersinfo = new Array();
for (i = 0; i < DI.length; i++) {
	var child = DI[i].firstChild;
	var email = '';
	var enroll = ''
	var lastvisit = '';
	while (child) {
		if (child.nodeName.toLowerCase() == 'a') {
			email = child.innerHTML;
		}
		if (child.nodeName.toLowerCase() == 'div') {
			var attr = child.getAttribute("class");
			if (attr == 'enroll-date') {
				enroll = child.innerHTML;
			}
			if (attr == 'last-visited') {
				lastvisit = child.innerHTML;
			}
		}
		child = child.nextSibling;
	}
	var thisUsrArray = ['"' + email + '"' + ';' + '"' + enroll + '"' + ';' + '"' + lastvisit + '"']
	usersinfo.push(thisUsrArray);
}
alert(usersinfo);
